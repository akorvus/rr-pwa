import { Component, OnInit, OnDestroy } from '@angular/core';
import { PromocionService } from '../../../services/promocion.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GeneralService } from '../../../services/general.service';
import { RadioService } from '../../../services/radio.service';



@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.css']
})
export class PromocionesComponent implements OnInit {

  promociones = [];
  list_promo = false;
  promo_active;
  promo_click = false;
  partcipando_active = true;
  participaciones;
  promo_select;
  emisora_select = 0;
  cargando = false;
  load_init=true;
  block_area=true;

  promociones_active=[];


  constructor(public _promocionService: PromocionService, public router: Router, public _generalService: GeneralService, public _radioService: RadioService) {

    this._generalService.collapse_promo = false;


  }

  ngOnInit() {

    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access != null) {


        this._promocionService.get_promos().subscribe((resp: any) => {
          this.load_init=false;
          this.block_area=false;

          this.promociones = resp['promociones'];
          this.participaciones = resp['participaciones'];
        }, error => {
          this.load_init=false;
          this.block_area=true;
          localStorage.clear();

         })
     }
    else{
          this.load_init=false;
    }
  }

  ngOnDestroy() {
    this._generalService.collapse_promo = true;
  }

  back_list() {
    this.promo_click = false;
    this.list_promo = true;
  }
  change_promocion(id_emisora)
  {
    this.emisora_select = id_emisora;
    this.list_promo=true;

      this.promociones_active = this.promociones.filter(promocion => promocion.id_emisora == id_emisora);


  }


  ver_promo(promo) {
    this.promo_click = true;
    this.promo_select = promo;
    this.partcipando_active = false;

    for (let parti of this.participaciones) {
      if (parti.id_promocion == promo.id) {
        this.partcipando_active = true;
      }
    }
  }
  participar(id_promocion) {
    this.cargando = true;
    this._promocionService.set_participacion(id_promocion).subscribe((resp: any) => {
      this.participaciones = resp['participaciones'];
      this.cargando = false;
      this.partcipando_active = true;

    }, error => {
      this.cargando = false;
      this.partcipando_active = true;
    })
  }

  imprime() {
    this.promo_click = true;
    this.list_promo = true;
    console.log(this.promo_click);
    console.log(this.list_promo);
  }

}
