import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';
import { GeneralService } from '../../services/general.service';
import swal from 'sweetalert2';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-podgcast',
  templateUrl: './podgcast.component.html',
  styleUrls: ['./podgcast.component.css']
})
export class PodgcastComponent implements OnInit {


  //locutores;
  podcasts;
  url_descarga;
  file_name;
  podcast_emisora;
  podcast_filtro;
  podcast_select;
  btn_descarga=false;
  ios=false;
  filtros=[];
  id_filtro ='';


  constructor(public _generealService:GeneralService, public _radioService: RadioService,private deviceService: DeviceDetectorService) { }

  ngOnInit() {
    let info = this.deviceService.getDeviceInfo()
    if(info.os=='iOS')
      {
      this.ios=true;
      }
      else
      {
      this.ios=false;
      }


    this._radioService.get_podcast().subscribe((resp: any) => {
      //console.log(resp);
      //this.locutores = resp['locutores'];
      this.podcasts = resp['podcasts'];
      this.filtros =resp['filtros'];
    })

    if(this._radioService.play_podcast)
    {
      this._radioService.list_podcast=true;
    }
  }

  // FILTRADO POR LOCUTOR * VIEJO
  // change_podcast(locutor) {
  //   let pods = [];
  //   for (let podcast of this.podcasts) {
  //     if (podcast.id_locutor == locutor.id) {
  //       pods.push(podcast);
  //     }
  //
  //   }
  //   this.podcast_active = pods;
  //   this._radioService.list_podcast = true;
  //   this._radioService.locutor_active = locutor;
  //   this._radioService.all_podcast = pods;
  //   this._radioService.stop_podcast();
  // }


  // FILTRADO POR EMISORA NUEVO
  change_podcast(id_emisora) {

    this.podcast_emisora = this.podcasts.filter(podcast => podcast.id_emisora == id_emisora);
    this.podcast_filtro = this.podcast_emisora;

    this._radioService.list_podcast = true;
    this._radioService.emisora_active = id_emisora;
    this._radioService.all_podcast = this.podcast_filtro;
    this._radioService.stop_podcast();
  }

  chage_filtro()
  {

    if(this.id_filtro=='')
    {
      this.podcast_filtro = this.podcast_emisora;
      this._radioService.all_podcast = this.podcast_filtro;
    }
    else{
        this.podcast_filtro = this.podcast_emisora.filter(podcast => podcast.id_filtro == this.id_filtro);
        this._radioService.all_podcast = this.podcast_filtro;
    }
  }

  reset_locutor()
  {
    this._radioService.list_podcast=false;

  }

  descargar(link)
  {
    //console.log('llega 1');
    this._generealService.download_file(link).subscribe((resp:any)=>{
      //console.log(resp);
        this.btn_descarga=true;

      let dt = new Date();

        this.file_name=dt.getTime()+".mp3";

    });
  }

  podcast_rep(item)
  {
    this.btn_descarga=false;
    this._radioService.reproducir_podcast(item);
    this.descargar(item.link);




  }


}
