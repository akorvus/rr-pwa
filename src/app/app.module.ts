import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material';

import { app_routing } from './app.routes';

//COMPONENTES
import { AccesoComponent } from './components/acceso/acceso.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { NoticiaComponent } from './components/noticia/noticia.component';
import { RadioComponent } from './components/radio/radio.component';
import { NavopcionComponent } from './components/navopcion/navopcion.component';
import { ProgramasComponent } from './components/programas/programas.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { VideoComponent } from './components/video/video.component';
import { MfooterComponent } from './components/mfooter/mfooter.component';
import { CnoticiasComponent } from './components/cnoticias/cnoticias.component';
import { PromocionesComponent } from './components/promocion/promociones/promociones.component';
import { MpromocionComponent } from './components/promocion/mpromocion/mpromocion.component';
import { EncuestasComponent } from './components/encuesta/encuestas/encuestas.component'


//IMAGE COMPRESS
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

//SOCIAL LOGIN
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import {Angular2UsefulSwiperModule} from "angular2-useful-swiper";

//VIRTUAL SCROLL

import { ScrollingModule } from "@angular/cdk/scrolling";



//GALERIA
import { CrystalGalleryModule } from 'ngx-crystal-gallery';



//ANGULAR PWA
import { ServiceWorkerModule, SwUpdate, SwPush } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material';
import { environment } from '../environments/environment';
import { EmisoraComponent } from './components/emisora/emisora.component';
import { PodgcastComponent } from './components/podgcast/podgcast.component';
import { CuponComponent } from './components/cupon/cupon.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { DomseguroPipe } from './pipes/domseguro.pipe';


//BOOSTRAR ANGULAR

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BtnPausaComponent } from './components/btn-pausa/btn-pausa.component';


 import { DeviceDetectorModule } from 'ngx-device-detector';
 import { DeviceDetectorService } from 'ngx-device-detector';


//import { getAuthServiceConfigs } from './socialloginConfig';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('347483481979-3t31qmaoi4go3sl0nmiufii2kdjfoce6.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('367063947351528')
  }
  /*{
    id: LinkedInLoginProvider.PROVIDER_ID,
    provider: new LinkedInLoginProvider('78iqy5cu2e1fgr')
  }*/
]);

export function provideConfig() {
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    AccesoComponent,
    LoginComponent,
    RegistroComponent,
    NoticiasComponent,
    NoticiaComponent,
    RadioComponent,
    NavopcionComponent,
    MfooterComponent,
    CnoticiasComponent,
    ProgramasComponent,
    GaleriaComponent,
    VideoComponent,
    EmisoraComponent,
    PodgcastComponent,
    CuponComponent,
    DomseguroPipe,
    PromocionesComponent,
    MpromocionComponent,
    EncuestasComponent,
    BtnPausaComponent
  ],
  imports: [
    BrowserModule,
    app_routing,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MaterialModule,
    SocialLoginModule,
    ScrollingModule,
    CrystalGalleryModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    Angular2UsefulSwiperModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
    AngularFirestoreModule,
    DeviceDetectorModule.forRoot(),
    ServiceWorkerModule.register('/combined-worker.js', { enabled: environment.production })
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: provideConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(update: SwUpdate, push: SwPush, snackBar: MatSnackBar , deviceService: DeviceDetectorService) {
    update.available.subscribe(update => {
      //console.log('Actualizaion Disponible');

      let info = deviceService.getDeviceInfo();

      if(info.os !='iOS')
      {
        const snack = snackBar.open('Hay una actualización', 'Actualizar');

        snack.onAction().subscribe(() => {
          window.location.reload();
        })
      }


    });
  }

}
