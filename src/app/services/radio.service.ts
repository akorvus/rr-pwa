import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { GeneralService } from './general.service';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class RadioService {

  url
  token = "";

  //NOTICIAS
  emisora_noticia =0;
  noticias=[];
  radio_noticias_primeravez = true;


  // FOR PODCASTS
  list_podcast = false;
  locutor_active; // LOCUTOR SELECCIONADO
  emisora_active;// EMISORA SELECCIONADA
  all_podcast;  //  TODOS LOS PODCAST FILTRADOS POR LOCUTOR
  podcast_select; // PODCAST ACTIVOS
  load_podcas = false; // ESTA CARGADO UN PODCASTS
  image_podcat = "assets/img/fondo_gris.png";
  play_podcast = false; // MOSTRAR PLAY / PAUSA
  podcast_player = false // MOSTRAR CONTROLES
  //emisoras_name = ['','TOP MUSIC - 91.7','LA Z - 97.1', 'LA JEFA - 98.7','91 DAT - 90.9','MÍA - 93.9','RR NOTICIAS - 91.7 '];

  emisoras_conf= [{},
    { fondo: 'assets/img/back_top.png', nombre: 'TOP MUSIC - 91.7',color:"#040402",tipo:"obscuro" },
  { fondo: 'assets/img/back_laz.png', nombre: 'LA Z - 97.1',color:"#d31f26",tipo:"obscuro"},
  { fondo: 'assets/img/back_jefa.png', nombre: 'LA JEFA - 98.7',color:"#d3a54f",tipo:"obscuro"},
  { fondo: 'assets/img/back_91.png', nombre: '91 DAT - 90.9 ',color:"#f9e009",tipo:"claro" },
  { fondo: 'assets/img/back_mia.png', nombre: 'MÍA - 93.9' ,color:"#1a5b9f",tipo:"obscuro"},
  { fondo: 'assets/img/back_rr.png', nombre: 'RR NOTICIAS - 91.7',color:"#fff",tipo:"claro"}
];


  //RADIO
  reproductor = false;
  radio_text = "";
  load = false;
  play_radio = false;
  image_emisora = "assets/img/fondo_gris.png";

  menu_footer = "radio";

  radios = {
    //top: 'http://18613.live.streamtheworld.com/XHKHFM.mp3',
    // top:'http://18613.live.streamtheworld.com/XHKHFMAAC.aac',
    top:'https://playerservices.streamtheworld.com/api/livestream-redirect/XHKHFMAAC.aac',
    dat:'https://playerservices.streamtheworld.com/api/livestream-redirect/XHQRTFMAAC.aac',
    // dat: "http://18613.live.streamtheworld.com/XHQRTFMAAC.aac",
    //dat: "http://16693.live.streamtheworld.com/XHQRTFM.mp3",
    //laz: "http://stream2.dyndns.org:8000/xhrq.mp3",
    //laz:"http://stream2.dyndns.org:8000/stream/135/",
    laz:"https://sts.aplradiorama.mx/xhrq.mp3",
    //mia: "http://stream2.dyndns.org:8000/xhhy.mp3",
    mia: "https://sts.aplradiorama.mx/xhhy.mp3",
  //  mia: "http://18613.live.streamtheworld.com/xhhy.mp3",
    //jefa: "http://stream2.dyndns.org:8000/xhmq.mp3",
    jefa: "https://sts.aplradiorama.mx/xhmq.mp3",

    rr: "https://playerservices.streamtheworld.com/api/livestream-redirect/XHKHFMAAC.aac"
  };

  // r_top=['http://playerservices.streamtheworld.com/api/livestream-redirect/XHKHFMAAC.aac'];
  // r_dat=['http://playerservices.streamtheworld.com/api/livestream-redirect/XHQRTFMAAC.aac'];
  // r_laz=['http://stream2.dyndns.org:8000/stream/135/','http://stream2.dyndns.org:8000/xhrq.mp3'];
  // r_mia=[];
  // r_fefa=[];

  textos = {
    top: "Estás escuchando: TOP MUSIC",
    dat: "Estás escuchando: 91 DAT",
    laz: "Estás escuchando: LA Z",
    mia: "Estás escuchando: MIA",
    jefa: "Estás escuchando: LA JEFA",
    rr: "Estás escuchando: RR NOTICIAS"
  }

  fondos = {
    top: "assets/img/back_top.png",
    dat: "assets/img/back_91.png",
    laz: "assets/img/back_laz.png",
    mia: "assets/img/back_mia.png",
    jefa: "assets/img/back_jefa.png",
    rr: "assets/img/back_rr.png"
  }


  audio = new Audio('http://18613.live.streamtheworld.com/XHKHFM.mp3');


  constructor(public $http: HttpClient, public _generealService: GeneralService,private sanitizer: DomSanitizer) {

    this.url = this._generealService.url;

  }


  get_podcast() {
    const query = this.url + '/api/app/podcast';
    return this.$http.get(query).pipe(map((resp: any) => {

      for (let podcast of resp['podcasts']) {
        podcast.imagen = this.url + '/podcast/' + podcast.imagen;
      }

      // for (let locutor of resp['locutores']) {
      //   locutor.imagen = this.url + '/locutor/' + locutor.imagen;
      // }

      return resp;
    }));
  }

  reproducir(item) {
    this.reproductor = true;
    // if (this.load == false) {
      this.reset_podcast();
      console.log('pasa')
      this.load = true;
      this.audio.pause();
      this.audio.src = this.radios[item];
      this.audio.play().then(() => {
        this.audio.muted = false;
        this.load = false
        this.play_radio = true;
        this.image_emisora = this.fondos[item];
        this.radio_text = this.textos[item];
      },error=>{
            this.image_emisora = this.fondos[item];
            this.radio_text="Emisora no disponible";
          this.load = false

        console.log('error');
      }
    );
    // }
    // else {
    //   console.log('no pasa')
    // }


  }

  pause_radio() {
    this.audio.muted = true;
    this.play_radio = false;
  }

  continue_radio() {
    this.audio.muted = false;
    this.load = false
    this.play_radio = true;

    /*this.audio.play().then(() => {
      this.load = false
      this.play_radio = true;
    });*/
  }


  pause_podcast() {
    this.audio.muted = true;
    this.play_podcast = false;
  }

  continue_podcast() {
    this.audio.muted = false;
    this.load_podcas = false
    this.play_podcast = true;
  }

  reproducir_podcast(item) {
    this.podcast_player = true;
    if (this.load_podcas == false) {
      this.reset_radio();
      this.load_podcas = true;
      this.audio.pause();
      console.log(item)
      this.audio.src = item.link;
      this.podcast_select = item;
      this.audio.play().then(() => {
        this.audio.muted = false;
        this.load_podcas = false
        this.play_podcast = true;
        this.image_podcat = item.imagen;
        this.radio_text = item.titulo + "-" + item.episodio;
      }).catch(()=>{
              this.load_podcas = false

              console.log('error');
              this.play_podcast = false;
      });




    }
    else {
      console.log('no pasa')


    }


  }


  convertDataURIToBinary(dataURI) {
    var BASE64_MARKER = ';base64,';
    var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    var base64 = dataURI.substring(base64Index);
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }



  stop_podcast() {
    // PODCAST CORIENDO
    if (this.reproductor == false) {
      this.audio.pause();
      this.radio_text = "";
      this.podcast_player = false;
    }
    this.image_podcat = this.emisoras_conf[this.emisora_active].fondo;


  }


  reset_radio() {
    this.reproductor = false;
    this.radio_text = "";
    this.load = false;
    this.play_radio = false;
    this.image_emisora = "assets/img/fondo_gris.png";
  }


  reset_podcast() {
    this.list_podcast = false;
    this.load_podcas = false; // ESTA CARGADO UN PODCASTS
    this.image_podcat = "assets/img/fondo_gris.png";
    this.play_podcast = false; // MOSTRAR PLAY / PAUSA
    this.podcast_player = false // MOSTRAR CONTROLES
  }


}
