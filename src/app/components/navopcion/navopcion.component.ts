import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RadioService } from '../../services/radio.service';
import { AccesoService } from '../../services/acceso.service';
import { GeneralService } from '../../services/general.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-navopcion',
  templateUrl: './navopcion.component.html',
  styleUrls: ['./navopcion.component.css']
})
export class NavopcionComponent implements OnInit {

  user_name = "Invitado";
  user_email = "";
  session = false;
  galeria_expand = true;
  promo_expand = true;

  constructor(public _accesoService: AccesoService,
     public _radioService: RadioService,
     public _generalService:GeneralService
    ,private deviceService: DeviceDetectorService) {

    this.galeria_expand=this._generalService.collapse_galeria;
    this.promo_expand=this._generalService.collapse_promo;

   }

  ngOnInit() {

    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access != null) {
      this.user_name = access.name;
      this.user_email = access.email;
      this.session = true;

    }


  }



  bye_session() {
    this._radioService.audio.pause();
    this._accesoService.bye();
  }

}
