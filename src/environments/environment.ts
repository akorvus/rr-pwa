// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDXi_EaPlM9xK22aSSK10esYz-kaYenfkI",
    authDomain: "pwa-rr-1554829527751.firebaseapp.com",
    databaseURL: "https://pwa-rr-1554829527751.firebaseio.com",
    projectId: "pwa-rr-1554829527751",
    storageBucket: "pwa-rr-1554829527751.appspot.com",
    messagingSenderId: "347483481979"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
