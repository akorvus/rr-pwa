import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url
  token = "";
  constructor(public $http: HttpClient,public _generealService: GeneralService) {

  this.url = this._generealService.url;

 }

  set_user(usuario) {
    const query = this.url + '/api/app/new-user?token=' + this.token;

    const item = JSON.stringify(usuario);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.$http.post(query, item, httpOptions);
  }
  set_user_social(usuario) {
    const query = this.url + '/api/app/new-user-social?token=' + this.token;

    const item = JSON.stringify(usuario);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.$http.post(query, item, httpOptions);
  }

  validate_email(data) {
    const query = this.url + '/api/app/validate-email';
    const item = JSON.stringify(data);
    return this.$http.post(query,data);
  }
}
