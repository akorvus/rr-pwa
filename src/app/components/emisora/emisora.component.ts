import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';

@Component({
  selector: 'app-emisora',
  templateUrl: './emisora.component.html',
  styleUrls: ['./emisora.component.css']
})
export class EmisoraComponent implements OnInit {

  galerias = [];
  img_emisora = "";
  list_emisoras = false;
  emisora;
  galeria_active;
  prog_click = false;
  galeria_select;

  emisoras = [
    { icon: 'assets/img/icon_91.png', nombre: '91 DAT - 90.9', id: 4,bg:"assets/img/bg_91.jpg" ,color:"#161616",descrip:"Estación líder de música Pop con mayor cobertura del Estado con más de 20 años de experiencia en el formato juvenil. Nuestro concepto energético y positivo  que cuida los valores familiares, hace que 7 de cada 10 jóvenes nos prefieran.",web:"https://91dat.com.mx/"},
    { icon: 'assets/img/icon_mia.png', nombre: 'MÍA - 93.9', id: 5 ,bg:"assets/img/bg_mia.jpg",color:"#fff",descrip:"Una estación llena de positivismo, con música que te llega directo al corazón y siempre tendrá el tip que necesitas para tu vida diaria, MIA se considera a sí misma como LA RADIO INTELIGENTE.",web:"http://miafm.mx/"},
    { icon: 'assets/img/icon_top.png', nombre: 'TOP MUSIC - 91.7', id: 1 ,bg:"assets/img/bg_top.jpg",color:"#fff",descrip:"Única estación en el Estado enfocada al segmento más atractivo del mercado. Tocando lo mejor de la música en inglés representada por sus más grandes hits, así como los éxitos del momento, es por esto que somos MÁS VARIEDAD, Y MUCHO MÁS MÚSICA.",web:"https://topmusic.mx/"},
    { icon: 'assets/img/icon_rr.png', nombre: 'RR NORICIAS - 91.7 ', id: 6 ,bg:"assets/img/bg_rr.jpg",color:"#fff",descrip:"Noticiero matutino a cargo de Ángel Solís a partir de las 5:30 de la mañana y Edgar Pliego a partir de la 1 de la tarde, que proporciona de manera completa y oportuna la información más relevante de lo que acontece en México y el mundo con un oportuno análisis y un toque con sentido del humor.",web:"http://rrnoticias.mx/"},
    { icon: 'assets/img/icon_z.png', nombre: 'LA Z - 97.1 ', id: 2 ,bg:"assets/img/bg_z.jpg",color:"#fff",descrip:"Un fenómeno de la radio que ha arrasado con la audiencia, ya que nuestro grupo es el único en manejar música grupera en FM. La Z es la estación con mayor rating de Querétaro y SALVAJEMENTE GRUPERA.",web:"http://lazqro.com.mx/"},
    { icon: 'assets/img/icon_jefa.png', nombre: 'LA JEFA - 98.7', id: 3 ,bg:"assets/img/bg_jefa.jpg",color:"#fff",descrip:"Frecuencia grupera que ofrece un toque especial con baladas del recuerdo de los 70’s y 80’s.",web:"http://www.lajefarr.mx/"}

  ];

  constructor(public _radioService: RadioService) { }

  ngOnInit() {
  }

  change_emisora(id_emisora) {
    this.list_emisoras = false;
    this.prog_click=true;
    for (let emi of this.emisoras) {
      if (emi.id == id_emisora) {
        this.emisora = emi;
      }
    }

  }

  reset_emisora() {
    this.list_emisoras = false;
  }
  back_list() {
    this.prog_click = false;
  }

}
