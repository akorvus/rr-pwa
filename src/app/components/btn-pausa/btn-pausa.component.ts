import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';

@Component({
  selector: 'app-btn-pausa',
  templateUrl: './btn-pausa.component.html',
  styleUrls: ['./btn-pausa.component.css']
})
export class BtnPausaComponent implements OnInit {

  constructor(public _radioService: RadioService) { }

  ngOnInit() {
  }

  play_pause() {
    if(this._radioService.play_radio==false && this._radioService.load==false) {
      this._radioService.continue_radio();
    } else if (this._radioService.play_radio==true  && this._radioService.load==false) {
      this._radioService.pause_radio()
    }
  }

}
