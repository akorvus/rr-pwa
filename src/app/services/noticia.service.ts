import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class NoticiaService {

  meses=['enero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
url
  token="";

  constructor(public $http: HttpClient,public _generealService: GeneralService) {

  this.url = this._generealService.url;

 }

  get_noticias() {
    const query = this.url + '/api/app/noticias';
    return this.$http.get(query).pipe(map((resp:any)=>{

      for(let noticia of resp['noticias'])
      {
        noticia.imagen = this.url+'/noticias/'+noticia.imagen;
      }

      return resp;
    }));
  }

  get_noticia(noticia)
  {
    let query = this.url + "/api/app/noticia-show/" + noticia;



    return this.$http.get(query).pipe(map((resp: any) => {
      //console.log(resp);
      let dt = new Date(resp.created_at);
      //console.log(dt)
      let fecha= dt.getDate()+' - '+this.meses[dt.getMonth()-1]+' - '+dt.getFullYear();

      resp.created_at=fecha;

      resp.imagen = this.url+'/noticias/'+resp.imagen;

      return resp;
    }));
  }



}
