import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NoticiaService } from '../../services/noticia.service';
import { RadioService } from '../../services/radio.service';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import swal from 'sweetalert2';

import Swiper from 'swiper';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  noticias = [];
  noticias_active = [];
  noticias_header = [];

  cargando = false;
  noticia_individual=false;
  noticia_active;
  noticas_list = false;
  ios=false;

  constructor(public _noticiaService: NoticiaService, private router: Router, public _radioService: RadioService,private deviceService: DeviceDetectorService) {
    if (this._radioService.emisora_noticia > 0) {
      this.change_noticia(this._radioService.emisora_noticia);
      this.noticas_list = true;
    }
    this.cargando=true;

  }



  ngOnInit() {

    let info = this.deviceService.getDeviceInfo()
    if(info.os=='iOS')
      {
      this.ios=true;
      }
      else
      {
      this.ios=false;
      }

      // LOAD NOTICIAS LOCALST

      const noti_local =JSON.parse(localStorage.getItem('not_local'));

      if(null !=noti_local )
      {
          this._radioService.noticias = noti_local;
          this.cargando=false;

          console.log('data desce local')
      }

      this._noticiaService.get_noticias().subscribe((resp: any) => {

              // swal('Ok', 'REGRESA DATA', 'success');
      //console.log(resp)
      this._radioService.noticias = resp['noticias'];
      this.cargando=false;

      console.log('data_desde api')
      localStorage.setItem('not_local', JSON.stringify(resp['noticias']));



      //this.noticias=resp['noticias'];
      //  this.noticias_header=this.noticias;
    },error=>{

          this.cargando=false;
            // swal('Ok', 'ERROR API', 'success');

    });

    setTimeout(()=> {
      var swiper = new Swiper('#slider_1', {
        pagination: {
          el: '#slider_1-pag',
          //dynamicBullets: true,
          //bulletClass:'bullet-normal',
          //bulletActiveClass:'bullet-activo'
        },
      });

    }, 10);

  }
  change_noticia(id_emisora) {
    this.cargando = true;
    // let noti_emisora = [];
    // // for (let noticia of this._radioService.noticias) {
    // //   if (noticia.id_emisora == id_emisora) {
    // //     noti_emisora.push(noticia);
    // //   }
    // // }
    // this.noticias_active = noti_emisora;
    this.noticias_active = this._radioService.noticias.filter(noticia => noticia.id_emisora == id_emisora);


    setTimeout(()=>{
      //  if (this.noticias_active.length > 0) {
          this.noticas_list = true;
          this.build_header();

          // reproduce radio solo si es la primera vez
          if(this._radioService.radio_noticias_primeravez) {
            switch(id_emisora) {
              case 1:
                this._radioService.reproducir( 'top');
                break;
              case 2:
                this._radioService.reproducir( 'laz');
                break;
              case 3:
                this._radioService.reproducir( 'jefa');
                break;
              case 4:
                this._radioService.reproducir( 'dat');
                break;
              case 5:
                this._radioService.reproducir( 'mia');
                break;
              case 6:
                this._radioService.reproducir( 'rr');
                break;
            }
            this._radioService.radio_noticias_primeravez = false;

          }

          if (this._radioService.emisora_noticia > 0) {
            this._radioService.emisora_noticia = id_emisora;
            // reproduce radio

          }
          else {
            this._radioService.emisora_noticia = id_emisora;
          }
        //}
    }, 1500);


  }

  reset_list() {
    this.noticas_list = false;
    setTimeout(()=> {
      var swiper = new Swiper('#slider_1', {
        pagination: {
          el: '#slider_1-pag',
          //dynamicBullets: true,
          //bulletClass:'bullet-normal',
          //bulletActiveClass:'bullet-activo'
        },
      });

    }, 10);
    this._radioService.emisora_noticia = 0;
  }

  reset_individual() {
    this.noticia_individual = false;
    setTimeout(()=> {
      var swiper2 = new Swiper('#slider_2', {
        pagination: {
          el: '#slider_2-pag',
          //dynamicBullets: true,
          //bulletClass:'bullet-normal',
          //bulletActiveClass:'bullet-activo'
        },
      });
            this.cargando=false;
    }, 5);
  }

  build_header() {

    this.noticias_header = [];
    if (this.noticias_active.length > 3) {
      for (let i = 0; i < 3; i++) {
        this.noticias_header.push(this.noticias_active[i]);
      }
      setTimeout(()=> {
        var swiper2 = new Swiper('#slider_2', {
          pagination: {
            el: '#slider_2-pag',
            //dynamicBullets: true,
            //bulletClass:'bullet-normal',
            //bulletActiveClass:'bullet-activo'
          },
        });
              this.cargando=false;
      }, 10);
    }
    else {
      this.noticias_header = this.noticias_active;
      setTimeout(()=> {
        var swiper2 = new Swiper('#slider_2', {
          pagination: {
            el: '#slider_2-pag',
            //dynamicBullets: true,
            //bulletClass:'bullet-normal',
            //bulletActiveClass:'bullet-activo'
          },
        });
              this.cargando=false;
      }, 10);
    }
  }

  ver_noticia(noticia) {
    this.noticia_active=noticia;
    this.noticia_individual=true;
  }


  // ver_noticia(noticia) {
  //   this.router.navigate(['noticias/noticia', noticia]);
  // }

}
