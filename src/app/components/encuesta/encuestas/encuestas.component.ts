import { Component, OnInit } from '@angular/core';
import { EncuestaService } from '../../../services/encuesta.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GeneralService } from '../../../services/general.service';
import { RadioService } from '../../../services/radio.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-encuestas',
  templateUrl: './encuestas.component.html',
  styleUrls: ['./encuestas.component.css']
})
export class EncuestasComponent implements OnInit {

  encuestas = [];
  list_promo = true;
  promo_active;
  promo_click = false;
  partcipando_active = true;
  participaciones;
  encuesta_select;
  cargando = false;
  load_init=true;
  block_area=true;
  encuesta_send:any={};

  constructor(public _encuestaService: EncuestaService, public router: Router, public _generalService: GeneralService, public _radioService: RadioService) {}

  ngOnInit() {

    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access != null) {

        this._encuestaService.get_encuestas().subscribe((resp: any) => {
              this.block_area=false;
        this.encuestas = resp['encuestas'];
        this.load_init=false;

      }, error => {
        this.load_init=false;
        this.block_area=true;
        localStorage.clear();


      });

    }
    else{
          this.load_init=false;
    }

  }


  back_list() {
    this.promo_click = false;
    this.list_promo = true;
  }

  ver_promo(encuesta) {
    this.promo_click = true;
    this.list_promo = false;

    this.encuesta_select = encuesta;
    this.partcipando_active = false;
    this.build_send();
  }

  build_send()
  {
    let encuesta = {
      id_encuesta:this.encuesta_select['encuesta'].id,
      preguntas:[]
    }

    for(let preg of this.encuesta_select.preguntas)
    {
      let pregunta={id_pregunta:preg.id_pregunta,id_respuesta:""};
      encuesta.preguntas.push(pregunta);
    }

    this.encuesta_send=encuesta;


  }

  send_encuesta()
  {

    this.cargando=true;

    if(this.valid_data())
    {
      this._encuestaService.set_voto(this.encuesta_send).subscribe((resp:any)=>{
        this.cargando=false;
        this.back_list();
        this.encuestas = resp['encuestas'];
      swal('GRACIAS', 'Tu participación fue registrada', 'success');

      })
    }
    else{
      this.cargando=false;
      swal('¡UPS!', 'Responde todas las preguntas para enviar', 'warning');
    }
  }

  valid_data()
  {
    for(let respuesta of this.encuesta_send.preguntas)
    {
      if(respuesta.id_respuesta=="")
      {
        return false;
      }
    }

    return true;
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }

}
