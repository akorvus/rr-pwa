import { Component, OnInit } from '@angular/core';
import { ProgramaService } from '../../services/programa.service';
import { RadioService } from '../../services/radio.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.css']
})
export class ProgramasComponent implements OnInit {

  programas=[];
  img_emisora="";
  list_emisoras=false;
  emisora;
  prog_active;
  prog_click=false;
  programa_select;
  emisora_select = 0;
  cargando=false;

  emisoras = [
    { icon: 'assets/img/icon_91.png', nombre: '91 DAT - 90.9',id:4 },
  { icon: 'assets/img/icon_mia.png', nombre: 'MÍA - 93.9',id:5},
  { icon: 'assets/img/icon_top.png', nombre: 'TOP MUSIC - 91.7',id:1},
  { icon: 'assets/img/icon_rr.png', nombre: 'RR NORICIAS - 91.7 ',id:6 },
  { icon: 'assets/img/icon_z.png', nombre: 'LA Z - 97.1 ',id:2},
  { icon: 'assets/img/icon_jefa.png', nombre: 'LA JEFA - 98.7',id:3}

];


  constructor(public _programaService:ProgramaService, public router:Router, public _radioService: RadioService) { }

  ngOnInit() {
    this.cargando=false;
    this._programaService.get_programas().subscribe((resp:any)=>{
      this.programas=resp['programas'];
      this.cargando=false;
    })
  }

  change_emisora(id_emisora)
  {
    this.emisora_select = id_emisora;
    this.list_emisoras=true;
      for(let emi of this.emisoras)
      {
        if(emi.id==id_emisora)
        {
          this.emisora=emi;
        }
      }

      this.prog_active=this.programas.filter(programa => programa.id_emisora==id_emisora);
      //console.log(this.programas);

  }

  reset_emisora()
  {
    this.list_emisoras=false;
  }
  back_list()
  {
    this.prog_click=false;
  }

  ver_programa(programa)
  {
    this.prog_click=true;
    this.programa_select=programa;
  }

}
