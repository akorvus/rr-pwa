import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';
import { Router } from '@angular/router';
import { PushnotificationService } from '../../services/pushnotification.service';
import {GeneralService} from '../../services/general.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-mfooter',
  templateUrl: './mfooter.component.html',
  styleUrls: ['./mfooter.component.css']
})
export class MfooterComponent implements OnInit {
  constructor(
    public _radioService: RadioService,
    private router: Router,
    public _pushNotification: PushnotificationService,
    public _general:GeneralService,
    public deviceService: DeviceDetectorService) { }

  ngOnInit() {

    this._pushNotification.getPermission();
    let info = this.deviceService.getDeviceInfo();

    // if (info.os!='iOS') {
    //     this._general.check_version();
    //   }

  }

  change_section(section, item) {
    this._radioService.menu_footer = section;

    switch (section) {
      case 'noticias':
        this.router.navigate(['/noticias']);
        break;
      case 'radio':
        this.router.navigate(['/radio']);
        break;
      case 'promo':
        console.log('PROMO')
        break;
      case 'cupon':
        console.log('CUPONM')
        break;
    }

  }

}
