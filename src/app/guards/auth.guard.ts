import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { catchError, map, tap } from 'rxjs/operators';
import { GeneralService } from "../services/general.service"
import swal from 'sweetalert2';
import { AuthService } from "angularx-social-login";
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angularx-social-login";


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public url;
  user: SocialUser;
  session = true;
  constructor(
    public $http: HttpClient,
    public _generalService: GeneralService,
    private router: Router,
    private socialAuthService: AuthService) {

    //console.log('entra guard')
    this.url = this._generalService.url;
  }

  canActivate() {

    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access == null) {
      //  console.log('no pasa 1')
      swal('', 'Para ingresar a esta sección inicia sesión', 'info');
      return false;
    }

    else {

      return this.check_session().then(() => true).catch(() => {

        let type = this._generalService.get_type();
        console.log(type);

        if (type == 'web' || type == 'google_web') {
        swal('', 'Para ingresar a esta sección inicia sesión', 'info');
          return false;
        }
        else {
          if (type == 'google') {
            return this.log_google().then(() => true).catch(() => {
            swal('', 'Para ingresar a esta sección inicia sesión', 'info');
              return false;
            });
          }
          if (type == 'facebook') {
            return this.log_facebook().then(() => true).catch(() => {
              swal('', 'Para ingresar a esta sección inicia sesión', 'info');
              return false;
            });
          }

        }

      });




    }



  }


  check_session() {

    let query = `${this.url}/api/check-${this._generalService.get_type()}?token=${this._generalService.get_token()}`;

    console.log(query);

    return new Promise((resolve, reject) => {
    //  console.log('va respuesta');
      this.$http.get(query).toPromise().then(resp => {

        resolve();
      }).catch(err => {
      //  console.log('error promesa')

        reject();
      });

    })

  }

  log_google() {
    return new Promise((resolve, reject) => {
      this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((usuario) => {
        //console.log(usuario);
        localStorage.clear();

        let data = { token: usuario.idToken, section: 4, name: usuario.name, email: usuario.email, type: 'google' };
        localStorage.setItem('a_t', JSON.stringify(data));

        resolve()


      }).catch((e) => {
        reject()
      });
    })
  }

  log_facebook() {
    return new Promise((resolve, reject) => {
      this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((usuario) => {


        localStorage.clear();

        let data = { token: usuario.authToken, section: 4, name: usuario.name, email: usuario.email, type: 'facebook' };
        localStorage.setItem('a_t', JSON.stringify(data));

        resolve()


      }).catch((e) => {
        reject()
      });
    })
  }





}
