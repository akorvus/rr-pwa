import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { RouterOutlet } from "@angular/router";
import { filter } from 'rxjs/operators';
//import {slider,fader} from "./route-animations";

declare var gtag;

@Component({
  selector: 'app-root',
  /*animations: [ // <-- add your animations here
    // fader,
     slider
    // transformer,
    //stepper
  ],*/
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cfront';

  constructor(router: Router) {
    const navEndEvent$ = router.events.pipe(
      filter(e => e instanceof NavigationEnd)
    );
    navEndEvent$.subscribe((e: NavigationEnd) => {
      gtag('config', 'UA-137728648-1', { 'page_path': e.urlAfterRedirects });
    });



  }

  onActivate(event) {
    window.scroll(0, 0);

  }




  /*  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }*/
}
