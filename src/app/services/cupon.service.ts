import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CuponService {

  url
  constructor(public $http: HttpClient, public _generealService: GeneralService) {

    this.url = this._generealService.url;

  }

  get_token() {
    let access = JSON.parse(localStorage.getItem('a_t'));
    return access.token;
  }

  get_cupones() {
    const query = this.url + '/api/app/cupon';
    return this.$http.get(query).pipe(map((resp: any) => {

      for (let cupon of resp['cupones']) {
        cupon.imagen = this.url + '/cupon/' + cupon.imagen;
      }

      return resp;
    }));
  }
}
