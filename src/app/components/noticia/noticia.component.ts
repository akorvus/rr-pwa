import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NoticiaService } from '../../services/noticia.service';
import { RadioService } from '../../services/radio.service';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.css']
})
export class NoticiaComponent implements OnInit {

  noticia:any={imagen:""};
  s_noticia:any=[];

  constructor(public _noticiaService: NoticiaService,private activated: ActivatedRoute, public _radioService: RadioService) { }

  ngOnInit() {
    let noticia_id ="";

    this.activated.params.subscribe(params => {
      noticia_id = params['id'];

    })


    this._noticiaService.get_noticia(noticia_id).subscribe((resp:any)=>{
      this.noticia=resp;
      this.s_noticia.push(resp);
      //console.log(this.s_noticia)
    })
  }

}
