import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

 // public url = 'http://localhost:8000';
 public url = "https://api.respuestaradiofonica.com";
  //  public url = "https://rback.kypcom.com";

  file_url
  collapse_galeria=true;
  collapse_promo=true;

  constructor(public $http: HttpClient,private sanitizer: DomSanitizer) { }

  get_token() {
    let access = JSON.parse(localStorage.getItem('a_t'));
    return access.token;
  }

  get_type()
  {
    let access = JSON.parse(localStorage.getItem('a_t'));
    return access.type;
  }
  get_email()
  {
    let access = JSON.parse(localStorage.getItem('a_t'));
    return access.email;
  }

  check_session()
  {
    let query = this.url + "/api/check-web?token=" + this.get_token();
    return this.$http.get(query)

  }

  check_version()
  {
    const query = this.url + '/api/version-app';
       this.$http.get(query).subscribe((resp:any)=>{
         let version = JSON.parse(localStorage.getItem('v_app'));

         if (version == null) {
           let obj_v = {'ver':resp};
            localStorage.setItem('v_app', JSON.stringify(obj_v));
            window.location.reload();
         }
         else{
           if(version.ver != resp)
           {
             let obj_v = {'ver':resp};
              localStorage.setItem('v_app', JSON.stringify(obj_v));
              window.location.reload();
           }
         }
       })
  }


  download_file(link)
  {

    return this.$http.get(link,{ responseType: 'arraybuffer' }).pipe(map((resp: any) => {

      let  blob = new Blob([resp], {  type: 'application/octet-stream'  });
      /*

        let  url = window.URL;

        let file = url.createObjectURL(blob);

        console.log(blob);
        console.log(file);*/

        /*const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = 'test.webm';
  document.body.appendChild(a);
  a.click();
  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);*/

//console.log(blob);
    this.file_url = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));

    return true;

    }));


  }
}
