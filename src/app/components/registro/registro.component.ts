import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Platform } from '@angular/cdk/platform';
import { DateTimeAdapter, OWL_DATE_TIME_LOCALE, OwlDateTimeIntl } from 'ng-pick-datetime';
import { NativeDateTimeAdapter } from 'ng-pick-datetime/date-time/adapter/native-date-time-adapter.class';

export class EspIntl extends OwlDateTimeIntl {
  cancelBtnLabel = 'Cancelar';

  /** A label for the set button */
  setBtnLabel = 'Confirmar';

}

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers: [{ provide: OWL_DATE_TIME_LOCALE, useValue: 'es' },
    {provide: DateTimeAdapter, useClass: NativeDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE, Platform]},
  { provide: OwlDateTimeIntl, useClass: EspIntl },]
})

export class RegistroComponent implements OnInit {

  constructor(public _userService: UserService, private router: Router) { }

  estados = [
    {
      "id": 1,
      "name": "Aguascalientes"
    },
    {
      "id": 2,
      "name": "Baja California"
    },
    {
      "id": 3,
      "name": "Baja California Sur"
    },
    {
      "id": 4,
      "name": "Campeche"
    },
    {
      "id": 5,
      "name": "Coahuila"
    },
    {
      "id": 6,
      "name": "Colima"
    },
    {
      "id": 7,
      "name": "Chiapas"
    },
    {
      "id": 8,
      "name": "Chihuahua"
    },
    {
      "id": 9,
      "name": "Distrito Federal"
    },
    {
      "id": 10,
      "name": "Durango"
    },
    {
      "id": 11,
      "name": "Guanajuato"
    },
    {
      "id": 12,
      "name": "Guerrero"
    },
    {
      "id": 13,
      "name": "Hidalgo"
    },
    {
      "id": 14,
      "name": "Jalisco"
    },
    {
      "id": 15,
      "name": "México"
    },
    {
      "id": 16,
      "name": "Michoacán"
    },
    {
      "id": 17,
      "name": "Morelos"
    },
    {
      "id": 18,
      "name": "Nayarit"
    },
    {
      "id": 19,
      "name": "Nuevo León"
    },
    {
      "id": 20,
      "name": "Oaxaca"
    },
    {
      "id": 21,
      "name": "Puebla"
    },
    {
      "id": 22,
      "name": "Querétaro"
    },
    {
      "id": 23,
      "name": "Quintana Roo"
    },
    {
      "id": 24,
      "name": "San Luis Potosí"
    },
    {
      "id": 25,
      "name": "Sinaloa"
    },
    {
      "id": 26,
      "name": "Sonora"
    },
    {
      "id": 27,
      "name": "Tabasco"
    },
    {
      "id": 28,
      "name": "Tamaulipas"
    },
    {
      "id": 29,
      "name": "Tlaxcala"
    },
    {
      "id": 30,
      "name": "Veracruz"
    },
    {
      "id": 31,
      "name": "Yucatán"
    },
    {
      "id": 32,
      "name": "Zacatecas"
    }
  ];

  user = { name: "", email: "", pass: "", date: "", genero: "", estado: "Querétaro" ,type:"WEB" };
  public dateTime: Date = null;

  ngOnInit() {
  }

  set_user() {
    if (this.validate_data()==true) {
      //console.log(this.user);

            // swal('Ok', 'VA registro', 'success');
      //VALIDATE EMAIL
      this._userService.validate_email(this.user).subscribe((resp: any) => {


              // swal('Ok', 'REGRESA DATOS', 'success');
        //console.log(resp)
        if (resp.existe == 'no') {
          this._userService.set_user(this.user).subscribe((resp: any) => {
            swal('Ok', 'Ya puedes ingresar con tus datos', 'success');
            this.router.navigate(['/acceso']);
          })
        }
        else {
          swal('Ups!', 'El email ya esta registrado', 'error');
          //console.log('existe')
        }
      },error=>{

        console.log(error);

        let error_txt = 'Error en registro'+error;

        swal('Ups!', error_txt , 'error');


      })
    }
  }

  validate_data() {

    if(this.dateTime==null)
    {
      swal('Ups!', 'Ingresa tu fecha de nacimiento', 'error');
      return false;
    }

    let mes = this.dateTime.getMonth() + 1;
    this.user.date = this.dateTime.getFullYear() + "-" + mes + "-" + this.dateTime.getDate();

    let formulario = new FormGroup({
      'name': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'pass': new FormControl('', [Validators.required]),
      'date': new FormControl('',[Validators.required]),
      'genero': new FormControl(''),
      'type':new FormControl('WEB'),
      'estado': new FormControl('', [Validators.required])
    });

    console.log(this.user)

    console.log(formulario.value);

    formulario.setValue(this.user);
    if (formulario.valid == true) {
      return true;
    }
    else {
      swal('Ups!', 'Ingresa todos los datos de forma correcta', 'error');
      return false;
    }
  }

}
