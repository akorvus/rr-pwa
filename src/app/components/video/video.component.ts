import { Component, OnInit,OnDestroy } from '@angular/core';
import { GaleriaService } from '../../services/galeria.service';
import { RadioService } from '../../services/radio.service';
import { filter } from 'rxjs/operators';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  galerias = [];
  img_emisora = "";
  list_emisoras = false;
  emisora;
  galeria_active;
  prog_click = false;
  galeria_select;
  emisora_select = 0;

  emisoras = [
    { icon: 'assets/img/icon_91.png', nombre: '91 DAT - 90.9', id: 4 },
    { icon: 'assets/img/icon_mia.png', nombre: 'MÍA - 93.9', id: 5 },
    { icon: 'assets/img/icon_top.png', nombre: 'TOP MUSIC - 91.7', id: 1 },
    { icon: 'assets/img/icon_rr.png', nombre: 'RR NORICIAS - 91.7 ', id: 6 },
    { icon: 'assets/img/icon_z.png', nombre: 'LA Z - 97.1 ', id: 2 },
    { icon: 'assets/img/icon_jefa.png', nombre: 'LA JEFA - 98.7', id: 3 }

  ];

  constructor(public _galeriaService: GaleriaService, public _generalService: GeneralService, public _radioService: RadioService) {

    this._generalService.collapse_galeria=false;

   }

  ngOnInit() {
    this._galeriaService.get_videos().subscribe((resp: any) => {
      this.galerias = resp['videos'];

    })
  }
  ngOnDestroy()
  {
    this._generalService.collapse_galeria=true;
  }

  change_emisora(id_emisora) {
    this.emisora_select = id_emisora;
    this.list_emisoras = true;
    for (let emi of this.emisoras) {
      if (emi.id == id_emisora) {
        this.emisora = emi;
      }
    }


    this.galeria_active = this.galerias.filter(gale => gale.id_emisora == id_emisora);


  }

  reset_emisora() {
    this.list_emisoras = false;
  }
  back_list() {
    this.prog_click = false;
  }
}
