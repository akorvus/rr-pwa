import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';
import Swiper from 'swiper';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.css']
})
export class RadioComponent implements OnInit {

  user_name="Invitado";

  emisoras = [
    { icon: 'assets/img/91_circular.png', nombre: '91 DAT - 90.9', descripcion: "Música pop con contenido energético, divertido y juvenil que cuida los valores familiares.",radio:'dat' },
  { icon: 'assets/img/93_circular.png', nombre: 'MÍA - 93.9', descripcion: "Dosis de positivismo con música y tips que te llegan directo al corazón. La radio INTELIGENTE." ,radio:'mia'},
  { icon: 'assets/img/top_circular.png', nombre: 'TOP MUSIC - 91.7', descripcion: "La mejor música en inglés representada por su más grandes hits y éxitos del momento." ,radio:'top'},
  { icon: 'assets/img/rr_circular.png', nombre: 'RR NOTICIAS - 91.7 ', descripcion: "Ángel Solis y Edgar Pliego proporcionan completa y oportunamente la información más relevante de México y el mundo.",radio:'rr' },
  { icon: 'assets/img/z_circular.png', nombre: 'LA Z - 97.1 ', descripcion: "Fenómeno de la radio con la mejor música grupera. SALVAJEMENTE Grupera." ,radio:'laz'},
  { icon: 'assets/img/jefa_circular.png', nombre: 'LA JEFA - 98.7', descripcion: "Música grupera y toques especiales con baladas del recuerdo de los 70's y 80's." ,radio:'jefa'}

];

  constructor(public _radioService: RadioService) { }

  ngOnInit() {
    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access != null) {
        this.user_name=access.name;


    }

    setTimeout(()=> {
      var swiper = new Swiper('#slider_1', {
        pagination: {
          el: '#slider_1-pag',
          //dynamicBullets: true,
          //bulletClass:'bullet-normal',
          //bulletActiveClass:'bullet-activo'
        },
      });

    }, 10);
  }

}
