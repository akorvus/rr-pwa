import { Component, OnInit ,OnDestroy} from '@angular/core';
import { GaleriaService } from '../../services/galeria.service';
import { RadioService } from '../../services/radio.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CrystalLightbox } from 'ngx-crystal-gallery';
import { GeneralService } from '../../services/general.service';


@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css']
})
export class GaleriaComponent implements OnInit {

  galerias=[];
  img_emisora="";
  list_emisoras=false;
  emisora;
  galeria_active;
  prog_click=false;
  galeria_select;
  emisora_select = 0;

  emisoras = [
    { icon: 'assets/img/icon_91.png', nombre: '91 DAT - 90.9',id:4 },
  { icon: 'assets/img/icon_mia.png', nombre: 'MÍA - 93.9',id:5},
  { icon: 'assets/img/icon_top.png', nombre: 'TOP MUSIC - 91.7',id:1},
  { icon: 'assets/img/icon_rr.png', nombre: 'RR NORICIAS - 91.7 ',id:6 },
  { icon: 'assets/img/icon_z.png', nombre: 'LA Z - 97.1 ',id:2},
  { icon: 'assets/img/icon_jefa.png', nombre: 'LA JEFA - 98.7',id:3}

];

myImages = [
	{
		preview: 'path_to_preview',
		full: 'path_to_full_image',
		width: 100, // used for masonry
		height: 100 // used for masonry
	}

];

myConfig = {
  masonry: true,
  masonryMaxHeight: 200,
  masonryGutter: 0,
  loop: true,
  backgroundOpacity: 0.85,
  animationDuration: 100,
  counter: true,
  lightboxMaxWidth: '100%'
};



  constructor(public _galeriaService:GaleriaService, public router:Router,public lightbox: CrystalLightbox,public _generalService:GeneralService, public _radioService: RadioService) {

    this._generalService.collapse_galeria=false;

   }

  ngOnInit() {
    this._galeriaService.get_photos().subscribe((resp:any)=>{
      this.galerias=resp['photos'];


    })
  }
  ngOnDestroy()
  {
    this._generalService.collapse_galeria=true;
  }

  change_emisora(id_emisora)
  {
    this.emisora_select = id_emisora;  
    this.list_emisoras=true;
      for(let emi of this.emisoras)
      {
        if(emi.id==id_emisora)
        {
          this.emisora=emi;
        }
      }


      this.galeria_active=this.galerias.filter(gale => gale.id_emisora==id_emisora);


      this.build_photos(this.galeria_active);


  }

  build_photos(galeria)
  {
    let new_imagenes=[];
    let cuenta=1;
    for(let item of galeria)
    {
      let obj=	{
      		preview: item.imagen,
      		full: item.imagen,
          index:cuenta,
      		width: 100, // used for masonry
      		height: 100 // used for masonry
      	}
        new_imagenes.push(obj);
        cuenta++;
    }
    this.myImages=new_imagenes;
  }

  open_photo(indice)
  {
    this.lightbox.open(this.myImages,{index:indice});
  }

  reset_emisora()
  {
    this.list_emisoras=false;
  }
  back_list()
  {
    this.prog_click=false;
  }

  ver_programa(galeria)
  {
    this.prog_click=true;
    this.galeria_select=galeria;
  }

}
