import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PromocionService {

  url
  meses = ['enero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
  constructor(public $http: HttpClient, public _generealService: GeneralService) {

    this.url = this._generealService.url;

  }



  get_promos() {
    const query = `${this.url}/api/promocion-${this._generealService.get_type()}/todos?token=${this._generealService.get_token()}`;

    const formData: FormData = new FormData();
    formData.append('email', this._generealService.get_email());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.$http.post(query, formData).pipe(map((resp: any) => {

      console.log(resp);
      for (let promo of resp['promociones']) {
        promo.imagen = this.url + '/promo/' + promo.imagen;
        promo.inicia = this.build_inicia(promo.inicia);
        promo.termina = this.build_termina(promo.termina);
      }

      return resp;
    }));
  }

  get_mis_promos() {
    const query = `${this.url}/api/promocion-${this._generealService.get_type()}/mis-promos?token=${this._generealService.get_token()}`;

    const formData: FormData = new FormData();
    formData.append('email', this._generealService.get_email());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.$http.post(query, formData).pipe(map((resp: any) => {

      for (let promo of resp['promociones']) {
        promo.imagen = this.url + '/promo/' + promo.imagen;
        promo.inicia = this.build_inicia(promo.inicia);
        promo.termina = this.build_termina(promo.termina);
      }

      return resp;
    }));
  }

  set_participacion(id_promo)
  {
    const query = `${this.url}/api/promocion-${this._generealService.get_type()}/participar?token=${this._generealService.get_token()}`;
    const formData: FormData = new FormData();
    formData.append('email', this._generealService.get_email());
    formData.append('id_promocion', id_promo);

    return this.$http.post(query, formData);
  }

  build_inicia(date) {
    let dt = new Date(date);
    let fecha = dt.getDate() + ' de ' + this.meses[dt.getMonth() - 1];
    return fecha;
  }

  build_termina(date) {
    let dt = new Date(date);
    let fecha = dt.getDate() + ' de ' + this.meses[dt.getMonth() - 1] + ' del ' + dt.getFullYear();
    return fecha;
  }



}
