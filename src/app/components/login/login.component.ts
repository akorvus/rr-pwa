import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { AccesoService } from "../../services/acceso.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  forma:FormGroup;
  cargando = false;
  invalido = false;
  errorfrac=false;
  constructor(public _accesoService: AccesoService,private router: Router) {

    this.forma = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required]),

    });

  }



  ngOnInit() {

  }
  check_access() {
    let credentials = {
      email: this.forma.value.email,
      password: this.forma.value.password
    }
    this.invalido = false;
    this.cargando = true;
    this.errorfrac = false;
    localStorage.clear();
    this._accesoService.generate_token(credentials).subscribe((resp: any) => {

      this.cargando = false;
    //  console.log(resp);
      localStorage.clear();
      localStorage.setItem('a_t', JSON.stringify(resp));

      this.router.navigate(['/radio']);


    }, error => {

      this.cargando = false;
      if (error.status === 401) {
        this.invalido = true;
      }
      if (error.status === 423) {
        this.errorfrac = true;
      }

    });

  }

}
