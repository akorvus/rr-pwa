import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class ProgramaService {

url
  token="";

  constructor(public $http: HttpClient,public _generealService: GeneralService) {

  this.url = this._generealService.url;

 }

  get_programas()
  {
    const query = this.url + '/api/app/programacion' ;
    return this.$http.get(query).pipe(map((resp:any)=>{

      for(let programa of resp['programas'])
      {
        programa.img_chica = this.url+'/programacion/'+programa.img_chica;
      }

      return resp;
    }));
  }

}
