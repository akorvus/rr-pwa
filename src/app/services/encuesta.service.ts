import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {

  url
  constructor(public $http: HttpClient, public _generealService: GeneralService) {

    this.url = this._generealService.url;

  }

  get_encuestas() {
    const query = `${this.url}/api/encuesta-${this._generealService.get_type()}/todos?token=${this._generealService.get_token()}`;

    const formData: FormData = new FormData();
    formData.append('email', this._generealService.get_email());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.$http.post(query, formData).pipe(map((resp: any) => {

      for (let encuesta of resp['encuestas']) {
        encuesta['encuesta'].imagen = this.url + '/encuesta/' + encuesta['encuesta'].imagen;
      }

      return resp;
    }));
  }

  set_voto(data) {
    const query = `${this.url}/api/encuesta-${this._generealService.get_type()}/participar?token=${this._generealService.get_token()}`;

    const formData: FormData = new FormData();
    formData.append('email', this._generealService.get_email());
    formData.append('datos', JSON.stringify(data));
    return this.$http.post(query, formData).pipe(map((resp: any) => {

      for (let encuesta of resp['encuestas']) {
        encuesta['encuesta'].imagen = this.url + '/encuesta/' + encuesta['encuesta'].imagen;
      }

      return resp;
    }));


  }


}
