import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class GaleriaService {

  url
  token = "";

  constructor(public $http: HttpClient,public _generealService: GeneralService) {

  this.url = this._generealService.url;

 }

  get_photos() {
    const query = this.url + '/api/app/galeria';
    return this.$http.get(query).pipe(map((resp: any) => {

      for (let photo of resp['photos']) {
        photo.imagen = this.url + '/galeria/' + photo.imagen;
      }

      return resp;
    }));
  }
  get_videos() {
    const query = this.url + '/api/app/video';
    return this.$http.get(query).pipe(map((resp: any) => {

      for (let photo of resp['videos']) {
        photo.imagen = this.url + '/video/' + photo.imagen;
      }

      return resp;
    }));
  }
}
