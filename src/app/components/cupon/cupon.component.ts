import { Component, OnInit } from '@angular/core';
import { CuponService } from '../../services/cupon.service';
import { RadioService } from '../../services/radio.service';
import { filter } from 'rxjs/operators';
import { GeneralService } from '../../services/general.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cupon',
  templateUrl: './cupon.component.html',
  styleUrls: ['./cupon.component.css']
})
export class CuponComponent implements OnInit {

  cupones=[];
  list_cupon=true;
  cupon_active;
  cupon_click=false;
  cupon_select;
  load_init=true;
  block_area=true;

  constructor(public _cuponService:CuponService, public router:Router, public _radioService: RadioService,public _generalService:GeneralService) { }

  ngOnInit() {

    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access != null) {

        this._cuponService.get_cupones().subscribe((resp: any) => {
        this.load_init=false;
          this.block_area=false;
          this.cupones = resp['cupones'];
        },
        error=>{
            this.block_area=true;
          this.load_init=false;
          localStorage.clear();
        }

      );

    }
    else{
          this.load_init=false;
    }
  }
back_list()
{
  this.cupon_click=false;
  this.list_cupon=true;
}

ver_cupon(cupon)
{
  this.cupon_click=true;
    this.list_cupon=false;
  this.cupon_select=cupon;
}




}
