import { Component, OnInit, OnDestroy } from '@angular/core';
import { PromocionService } from '../../../services/promocion.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GeneralService } from '../../../services/general.service';

@Component({
  selector: 'app-mpromocion',
  templateUrl: './mpromocion.component.html',
  styleUrls: ['./mpromocion.component.css']
})
export class MpromocionComponent implements OnInit {

  promociones = [];
  list_promo = true;
  promo_active;
  promo_click = false;
  partcipando_active = true;
  participaciones;
  promo_select;

  load_init=true;
  block_area=true;


  constructor(public _promocionService: PromocionService, public router: Router, public _generalService: GeneralService) {

    this._generalService.collapse_promo = false;

  }

  ngOnInit() {
    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access != null) {


            this._promocionService.get_mis_promos().subscribe((resp: any) => {
            this.block_area=false;
              this.load_init=false;
              this.promociones = resp['promociones'];
            }, error => {
              this.block_area=true;
              this.load_init=false;
              localStorage.clear();
            })
          }
    else{
          this.load_init=false;
    }


  }
  ngOnDestroy() {
    this._generalService.collapse_promo = true;
  }

  back_list() {
    this.promo_click = false;
    this.list_promo = true;
  }

  ver_promo(promo) {
    this.promo_click = true;
    this.list_promo = false;
    this.promo_select = promo;



  }
}
