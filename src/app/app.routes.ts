import { RouterModule, Routes } from '@angular/router';
import {
  AccesoComponent,
  RegistroComponent,
  LoginComponent,
  NoticiasComponent,
  RadioComponent,
  CnoticiasComponent,
  NoticiaComponent,
  GaleriaComponent,
  VideoComponent,
  ProgramasComponent,
  EmisoraComponent,
  PodgcastComponent,
  CuponComponent,
  PromocionesComponent,
  MpromocionComponent,
  EncuestasComponent

} from './components/index.paginas';

import {AuthGuard}  from "./guards/auth.guard";

const app_routes: Routes = [
  { path: 'acceso', component: AccesoComponent, data: { animation: 'isLeft' } },//GENERAL
  { path: 'registro', component: RegistroComponent, data: { animation: 'isRight' } },//GENERAL
  { path: 'login', component: LoginComponent, data: { animation: 'isRight' } },//GENERAL
  {//RUTAS DE NOTICIAS
    path: 'noticias', component: CnoticiasComponent, children: [
      { path: 'todas', component: NoticiasComponent, data: { animation: 'isRight' } },//GENERAL
      { path: 'noticia/:id', component: NoticiaComponent, data: { animation: 'isRight' } },//GENERAL
      { path: '**', pathMatch: 'full', redirectTo: 'todas', data: { animation: 'isLeft' } }, // GENERAL
      { path: '', pathMatch: 'full', redirectTo: 'todas', data: { animation: 'isLeft' } } // GENERAL
    ], data: { animation: 'isLeft' }
  },//GENERAL
  {// RUTAS DE GALERIA
    path: 'galeria', component: CnoticiasComponent, children: [
      { path: 'photo', component: GaleriaComponent, data: { animation: 'isRight' } },//GENERAL
      { path: 'video', component: VideoComponent, data: { animation: 'isRight' } },//GENERAL
      { path: '**', pathMatch: 'full', redirectTo: 'photos', data: { animation: 'isLeft' } }, // GENERAL
      { path: '', pathMatch: 'full', redirectTo: 'photos', data: { animation: 'isLeft' } } // GENERAL
    ], data: { animation: 'isLeft' }
  },
  {// RUTAS DE GALERIA
    path: 'programacion', component: CnoticiasComponent, children: [
      { path: 'todas', component: ProgramasComponent, data: { animation: 'isRight' } },//GENERA
      { path: '**', pathMatch: 'full', redirectTo: 'todas', data: { animation: 'isLeft' } }, // GENERAL
      { path: '', pathMatch: 'full', redirectTo: 'todas', data: { animation: 'isLeft' } } // GENERAL
    ], data: { animation: 'isLeft' }
  },
  {// RUTAS DE ENCUESTA
    path: 'encuesta', component: CnoticiasComponent, children: [
      { path: 'todas', component: EncuestasComponent},//GENERA
      { path: '**', pathMatch: 'full', redirectTo: 'todas', data: { animation: 'isLeft' } }, // GENERAL
      { path: '', pathMatch: 'full', redirectTo: 'todas', data: { animation: 'isLeft' } } // GENERAL
    ], data: { animation: 'isLeft' }
  },
  { path: 'emisoras', component: EmisoraComponent, data: { animation: 'isRight' } },//GENERAL
  { path: 'cupon', component: CuponComponent },
  { path: 'podcast', component: PodgcastComponent, data: { animation: 'isRight' } },//GENERAL
  { path: 'radio', component: RadioComponent, data: { animation: 'isRight' } },//GENERAL

  { path: 'promociones', component: PromocionesComponent },//LOGUEADO
  { path: 'mispromo', component: MpromocionComponent },//LOGUEADO

  { path: '', pathMatch: 'full', redirectTo: 'acceso', data: { animation: 'isLeft' } }, // GENERAL
  { path: '**', pathMatch: 'full', redirectTo: 'acceso', data: { animation: 'isLeft' } } // GENERAL
];

export const app_routing = RouterModule.forRoot(app_routes, { useHash: true });
