import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireMessaging } from '@angular/fire/messaging';

import * as firebase from 'firebase/app';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class PushnotificationService {

  url
  constructor(public http: HttpClient, public afMessaging: AngularFireMessaging,public _generealService: GeneralService) {

  this.url = this._generealService.url;

 }

  getPermission() {

    //this.messaging.usePublicVapidKey(this.api_publica);
    this.afMessaging.requestToken
      .subscribe(
        (generado) => {
        //  console.log('Permission granted! Save to the server!', generado);
          this.update_token(generado);
        },
        (error) => { console.error(error); },
    );

  }

  update_token(token_generado) {

    let access = JSON.parse(localStorage.getItem('a_t'));
    let data ={user:'no',email:''};
    if (access != null) {
      data.user="si";
      data.email=access.email;
    }

    let query = this.url + "/api/app/update-token";
    const formData: FormData = new FormData();
    formData.append('generado', token_generado);
    formData.append('user',data.user);
    formData.append('email',data.email);
    this.http.post(query, formData).subscribe((resp) => {

      //console.log(resp);
    }, error => {

      console.log(error);
    });
  }


}
