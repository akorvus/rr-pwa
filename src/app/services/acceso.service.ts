import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GeneralService } from './general.service';

import { AuthService } from "angularx-social-login";
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angularx-social-login";


@Injectable({
  providedIn: 'root'
})
export class AccesoService {

  url
  loading = false;
    user: SocialUser;


  constructor(public $http: HttpClient, private router: Router, public _generalService: GeneralService,private socialAuthService: AuthService) {

    this.url = this._generalService.url;
  }

  generate_token(credentials) {
    let query = this.url + "/api/signin";
    let item = JSON.stringify(credentials);
    //console.log(item);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    //console.log(item);

    return this.$http.post(query, item, httpOptions);
  }

  bye() {
    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access == null) {

      this.router.navigate(['/acceso']);

    }

    if (access.type == 'web') {
      let query = this.url + "/api/bye?token=" + access.token;
      this.$http.get(query).subscribe((resp: any) => {
        localStorage.clear();
        //console.log('salio')
        this.router.navigate(['/acceso']);
      }, error => {
      //  console.log('error')
        localStorage.clear();
        this.router.navigate(['/acceso']);
      })
    }
    else {
      localStorage.clear();
    //  console.log('salio')
      this.router.navigate(['/acceso']);
    }


  }

  check_session() {
    this.loading = true;
    let access = JSON.parse(localStorage.getItem('a_t'));

    if (access == null) {
      this.loading = false;
      this.router.navigate(['/acceso']);

    } else {
      let token = access.token;
      let query = "";
      if (access.type == "web") {
        query = this.url + "/api/check-web?token=" + token;

        this.$http.get(query).subscribe((resp: any) => {
            this.loading = false
            this.router.navigate(['/noticias']);

          }, error => {

            this.loading = false

          });

      }
      else{
        this.loading = false
        this.router.navigate(['/noticias']);
      }
      /*if (access.type == "google") {
        query = this.url + "/api/check-google?token=" + token;
      }
      if (access.type == "facebook") {
        query = this.url + "/api/check-facebook?token=" + token;
      }*/

    /*  const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

    /*  this.$http.get(query).subscribe((resp: any) => {
        this.loading = false
        this.router.navigate(['/radio']);

      }, error => {


        if (access.type == "web") {
          this.loading = false
          this.router.navigate(['/acceso']);
        }
        if (access.type == "google") {
          this.log_google().then((ok)=>{

            this.loading = false
            this.router.navigate(['/radio']);

          }).catch(err=>{

            this.loading = false
            this.router.navigate(['/acceso']);

          })
        }
        if (access.type == "facebook") {

          this.log_facebook().then((ok)=>{

            this.loading = false
            this.router.navigate(['/radio']);

          }).catch(err=>{

            this.loading = false
            this.router.navigate(['/acceso']);

          })

        }


      });*/

    }


  }

  log_google() {
    return new Promise((resolve, reject) => {
      this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((usuario) => {
        //console.log(usuario);
        localStorage.clear();

        let data = { token: usuario.idToken, section: 4, name: usuario.name, email: usuario.email, type: 'google' };
        localStorage.setItem('a_t', JSON.stringify(data));

        resolve()


      }).catch((e) => {
        reject()
      });
    })
  }

  log_facebook() {
    return new Promise((resolve, reject) => {
      this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((usuario) => {


        localStorage.clear();

        let data = { token: usuario.authToken, section: 4, name: usuario.name, email: usuario.email, type: 'facebook' };
        localStorage.setItem('a_t', JSON.stringify(data));

        resolve()


      }).catch((e) => {
        reject()
      });
    })
  }


}
