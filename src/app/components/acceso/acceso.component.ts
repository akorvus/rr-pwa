import { Component, OnInit } from '@angular/core';
import { AuthService } from "angularx-social-login";
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angularx-social-login";
import { AccesoService } from "../../services/acceso.service";
import { UserService } from "../../services/user.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-acceso',
  templateUrl: './acceso.component.html',
  styleUrls: ['./acceso.component.css']
})
export class AccesoComponent implements OnInit {



  constructor(public _accesoService: AccesoService,
    private socialAuthService: AuthService,
    public _userService: UserService,
    public router: Router
  ) { }
  user: SocialUser;
  cargando_social=false;
  ngOnInit() {
    this._accesoService.check_session();

    /*this.socialAuthService.authState.subscribe((user) => {
      console.log(user);
      this.user = user;
      //this.loggedIn = (user != null);
    });*/

  }

  signInWithGoogle(): void {
      this.cargando_social=true;
    //console.log('subscribe')
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((usuario) => {
      //console.log(usuario);
      localStorage.clear();

      let data = {token_web:usuario.idToken, token:"" , section: 4, name: usuario.name, email: usuario.email, type: 'google_web' };


      this._userService.validate_email(data).subscribe((resp: any) => {

        if (resp.existe == 'no') {
          this._userService.set_user_social(data).subscribe((resp: any) => {
            data.token=resp['token'];
            localStorage.setItem('a_t', JSON.stringify(data));
            this._accesoService.loading=false;
              this.cargando_social=false;
            this.router.navigate(['/noticias']);

          })
        }
        else {
          data.token=resp['token'];
          localStorage.setItem('a_t', JSON.stringify(data));
            this._accesoService.loading=false;
            this.cargando_social=false;
          this.router.navigate(['/noticias']);
        }
      })


    }).catch((e) => {
        this._accesoService.loading=false;
        this.cargando_social=false;

    });
  }

  signInWithFB(): void {
          this.cargando_social=true;
  this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((usuario) => {


    localStorage.clear();

    let data = { token:"",token_web: usuario.authToken, section: 4, name: usuario.name, email: usuario.email, type: 'facebook_web' };

    this._userService.validate_email(data).subscribe((resp: any) => {
      //console.log(resp)
      if (resp.existe == 'no') {
        this._userService.set_user_social(data).subscribe((resp: any) => {
          data.token=resp['token'];
          localStorage.setItem('a_t', JSON.stringify(data));
          this._accesoService.loading=false;
            this.cargando_social=false;
          this.router.navigate(['/noticias']);

        })
      }
      else {
        data.token=resp['token'];
        localStorage.setItem('a_t', JSON.stringify(data));
          this._accesoService.loading=false;
          this.cargando_social=false;
        this.router.navigate(['/noticias']);
      }

    })

  }).catch((e) => {
      this._accesoService.loading=false;
          this.cargando_social=false;
  });
}




}
